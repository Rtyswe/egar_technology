package org.example.models;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "files")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class File extends BaseEntity {

    @Column(name = "lecture_id")
    private Long lectureId;

    @Column(name = "filename")
    private String filename;

    @Lob
    @Column(name = "content")
    private byte[] content;

    @Column(name = "filetype")
    private String filetype;

    public File(String name, String type, byte[] data, Long lectureId) {
        filename = name;
        filetype = type;
        content = data;
        this.lectureId = lectureId;
    }
}
