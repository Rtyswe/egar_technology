package org.example.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "lectures")
@Data
public class Lecture extends BaseEntity{

    @Column(name = "topic")
    private String topic;

    @Column(name = "lecture_date")
    private Date date;

    @Column(name = "teacher_id")
    private Long teacherId;

    @Column(name = "access")
    private boolean access;
}
