package org.example.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "tests")
@Data
public class Test extends BaseEntity {

    @Column(name = "lecture_id")
    private Long lectureId;

    @Column(name = "question")
    private String question;
}
