package org.example.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "answers")
@Data
public class Answer extends BaseEntity {

    @Column(name="student_id")
    private Long studentId;

    @Column(name = "test_id")
    private Long testId;

    @Column(name = "answer_date")
    private Date answerDate;

    @Column(name = "answer")
    private String text;
}
