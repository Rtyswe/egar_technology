package org.example.repositories;

import org.example.models.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AnswerRepo extends JpaRepository<Answer, Long> {

    @Override
    Optional<Answer> findById(Long id);

    Optional<Answer> findAnswerByStudentIdAndTestId(Long studentId, Long testId);

    @Query(value = "SELECT DISTINCT student_id FROM tests, answers " +
            "WHERE tests.lecture_id = :lecture_id AND tests.id = answers.test_id",
            nativeQuery = true)
    Optional<List<Long>> findUsersAnsweredTestsOfLecture(@Param("lecture_id") Long lectureId);

    @Query(value = "SELECT answers.id FROM tests, answers, users " +
            "WHERE tests.lecture_id = :lecture_id AND tests.id = answers.test_id AND users.id = :student_id",
            nativeQuery = true)
    Optional<List<Long>> findAnswersUserOfLecture(@Param("student_id") Long studentId, @Param("lecture_id") Long lectureId);
}
