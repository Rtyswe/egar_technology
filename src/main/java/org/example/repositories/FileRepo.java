package org.example.repositories;

import org.example.models.File;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FileRepo extends JpaRepository<File, Long> {

    Optional<File> findByLectureId(Long lectureId);
}
