package org.example.exceptions;

public class LectureCreateException extends Exception {

    public LectureCreateException(String mes) { super(mes); }
}
