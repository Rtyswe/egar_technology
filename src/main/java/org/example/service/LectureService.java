package org.example.service;

import org.example.dtos.LectureCreateDto;
import org.example.dtos.LectureDto;
import org.example.exceptions.LectureCreateException;

import java.util.List;

public interface LectureService {

    LectureDto findById(Long id);

    void create(LectureCreateDto lecture, Long teacherId) throws LectureCreateException;

    List<LectureDto> getMyLectures(Long id);

    List<LectureDto> getAllLectures();

    void updateAccess(Long lectureId, boolean access);
}
