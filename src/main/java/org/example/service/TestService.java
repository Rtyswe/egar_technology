package org.example.service;

import org.example.dtos.TestCreateDto;
import org.example.dtos.TestDto;

import java.util.List;

public interface TestService {

    void create(List<TestCreateDto> tests, Long lectureId);

    List<TestDto> findAllByLecture(Long id);
}
