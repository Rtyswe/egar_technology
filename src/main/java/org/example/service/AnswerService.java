package org.example.service;

import javassist.NotFoundException;
import org.example.dtos.AnswerDto;
import org.example.dtos.StudentAnswerDto;
import org.example.dtos.UserDto;

import java.util.List;

public interface AnswerService {

    void saveAnswer(StudentAnswerDto answerDto, Long studentId, Long testId);

    List<UserDto> getStudentsAnswered(Long lectureId) throws NotFoundException;

    List<AnswerDto> getAnswersStudentOfLecture(Long id, Long lectureId) throws NotFoundException;
}
