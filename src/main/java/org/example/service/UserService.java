package org.example.service;

import org.example.models.User;

public interface UserService {

    void register(User user);

    User findByUsername(String username);

    User findById(Long id);

    Long findIdByUsername(String username);
}
