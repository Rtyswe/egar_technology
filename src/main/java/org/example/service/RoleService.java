package org.example.service;

import org.example.models.Role;

public interface RoleService {
    Role getStudentRole();
}
