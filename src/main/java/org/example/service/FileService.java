package org.example.service;

import org.example.dtos.FileDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileService {

    void store(MultipartFile file, Long lectureId) throws IOException;

    FileDto getFileByLecture(Long lectureId) throws FileNotFoundException;
}
