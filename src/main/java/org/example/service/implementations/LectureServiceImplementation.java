package org.example.service.implementations;

import org.example.dtos.LectureCreateDto;
import org.example.dtos.LectureDto;
import org.example.exceptions.LectureCreateException;
import org.example.models.Lecture;
import org.example.repositories.LectureRepo;
import org.example.service.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LectureServiceImplementation implements LectureService {

    private final LectureRepo lectureRepo;

    @Autowired
    public LectureServiceImplementation(LectureRepo lectureRepo) {
        this.lectureRepo = lectureRepo;
    }

    @Override
    public LectureDto findById(Long id) {
        var lecture = lectureRepo.findById(id).orElse(null);
        if (lecture == null) {
            throw new BadCredentialsException("There is no lecture with this id");
        }

        return LectureDto.fromLecture(lecture);
    }

    @Override
    public void create(LectureCreateDto lectureCreateDto, Long teacherId) throws LectureCreateException {
        var lecture = lectureCreateDto.toLecture(teacherId);

        if (lectureRepo.findByTopic(lecture.getTopic()).isPresent()) {
            throw new LectureCreateException("Lecture with same topic exists");
        }

       lectureRepo.save(lecture);
    }

    @Override
    public List<LectureDto> getMyLectures(Long id) {
        List<Lecture> result = lectureRepo.findAllByTeacherId(id).orElse(null);
        if (result == null) {
            throw new BadCredentialsException("The teacher with this id has no lectures");
        }

        return result.stream().map(LectureDto::fromLecture).collect(Collectors.toList());
    }

    @Override
    public List<LectureDto> getAllLectures() {
        var result = lectureRepo.findAll();
        if (result.isEmpty()) {
            throw new BadCredentialsException("There are no lectures");
        }

        return result.stream().map(LectureDto::fromLecture).collect(Collectors.toList());
    }

    @Override
    public void updateAccess(Long lectureId, boolean access) {
        var lecture =  lectureRepo.findById(lectureId);
        if (lecture.isEmpty()) {
            throw new BadCredentialsException("There is no lecture with this id");
        }
        lecture.get().setAccess(access);
    }
}
