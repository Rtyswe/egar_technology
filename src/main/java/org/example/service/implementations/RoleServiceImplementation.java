package org.example.service.implementations;

import org.example.models.Role;
import org.example.repositories.RoleRepo;
import org.example.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImplementation implements RoleService {

    private final RoleRepo roleRepo;

    @Autowired
    public RoleServiceImplementation(RoleRepo roleRepo) {
        this.roleRepo = roleRepo;
    }

    @Override
    public Role getStudentRole() {
        return roleRepo.findByName("STUDENT").orElse(null);
    }
}
