package org.example.service.implementations;

import org.example.exceptions.UserAlreadyExistAuthenticationException;
import org.example.models.Role;
import org.example.models.User;
import org.example.repositories.RoleRepo;
import org.example.repositories.UserRepo;
import org.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImplementation implements UserService {

    private final UserRepo userRepo;
    private final RoleRepo roleRepo;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImplementation(UserRepo userRepo, RoleRepo roleRepo, BCryptPasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.roleRepo = roleRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void register(User user) {
        if (userRepo.findByUsername(user.getUsername()) != null) {
            throw new UserAlreadyExistAuthenticationException("User already exists");
        }
        if (user.getPassword().length() != 4) {
            throw new BadCredentialsException("The pin code must consist of 4 digits");
        }
        if (!user.getPassword().matches("^-?\\d+$")) {
            throw new BadCredentialsException("The pin-code must consist of digits");
        }
        if (user.getFirstName().length() < 1 || user.getLastName().length() < 1) {
            throw new BadCredentialsException("The first and last name should not be empty");
        }

        var roleStudent = roleRepo.findByName("STUDENT");
        List<Role> userRoles = new ArrayList<>();
        userRoles.add(roleStudent.orElse(null));

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(userRoles);

        userRepo.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public User findById(Long id) {
        return userRepo.findById(id).orElse(null);
    }

    @Override
    public Long findIdByUsername(String username) {
        return userRepo.findByUsername(username).getId();
    }
}
