package org.example.service.implementations;

import org.example.dtos.TestCreateDto;
import org.example.dtos.TestDto;
import org.example.repositories.LectureRepo;
import org.example.repositories.TestRepo;
import org.example.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TestServiceImplementation implements TestService {

    private final TestRepo testRepo;
    private final LectureRepo lectureRepo;

    @Autowired
    public TestServiceImplementation(TestRepo testRepo, LectureRepo lectureRepo) {
        this.testRepo = testRepo;
        this.lectureRepo = lectureRepo;
    }

    @Override
    public void create(List<TestCreateDto> tests, Long lectureId) {
        if (tests.isEmpty()) {
            throw new BadCredentialsException("Nothing to save");
        } else if (lectureRepo.findById(lectureId).isEmpty()) {
            throw new BadCredentialsException("The lecture for which you want to create tests does not exist");
        }
        for (TestCreateDto test : tests) {
            testRepo.save(test.toTest(lectureId));
        }
    }

    @Override
    public List<TestDto> findAllByLecture(Long lectureId) {
        var tests =  testRepo.findByLectureId(lectureId);
        if (lectureRepo.findById(lectureId).isEmpty()) {
            throw new BadCredentialsException("There is no lecture with this id");
        } else if (tests.isEmpty()) {
            throw new BadCredentialsException("There are no tests for this lecture");
        }

        return tests.stream().map(TestDto::fromTest).collect(Collectors.toList());
    }
}
