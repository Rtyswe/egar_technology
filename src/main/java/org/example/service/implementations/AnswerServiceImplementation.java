package org.example.service.implementations;

import javassist.NotFoundException;
import org.example.dtos.AnswerDto;
import org.example.dtos.StudentAnswerDto;
import org.example.dtos.UserDto;
import org.example.repositories.AnswerRepo;
import org.example.repositories.UserRepo;
import org.example.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnswerServiceImplementation implements AnswerService {

    private final AnswerRepo answerRepo;
    private final UserRepo userRepo;

    @Autowired
    public AnswerServiceImplementation(AnswerRepo answerRepo, UserRepo userRepo) {
        this.answerRepo = answerRepo;
        this.userRepo = userRepo;
    }

    @Override
    public void saveAnswer(StudentAnswerDto answerDto, Long studentId, Long testId) {
        if (answerRepo.findAnswerByStudentIdAndTestId(studentId, testId).isPresent()) {
            throw new BadCredentialsException("You can't answer the test again");
        }
        answerRepo.save(answerDto.toAnswer(studentId, testId));
    }

    @Override
    public List<UserDto> getStudentsAnswered(Long lectureId) throws NotFoundException {
        List<Long> studentsId = answerRepo.findUsersAnsweredTestsOfLecture(lectureId).orElse(null);
        if (studentsId == null || studentsId.isEmpty()) {
            throw new NotFoundException("No one has answered the test yet");
        }
        var studentAnswered = new ArrayList<UserDto>();
        for (Long studentId : studentsId) {
            var student = userRepo.findById(studentId);
            student.ifPresent(user -> studentAnswered.add(UserDto.fromUser(user)));
        }
        if (studentAnswered.isEmpty()) {
            throw new NotFoundException("");
        }
        return studentAnswered;
    }

    @Override
    public List<AnswerDto> getAnswersStudentOfLecture(Long id, Long lectureId) throws NotFoundException {
        List<Long> answersId = answerRepo.findAnswersUserOfLecture(id, lectureId).orElse(null);
        if (answersId == null || answersId.isEmpty()) {
            throw new NotFoundException("This student did not answer the test");
        }
        var answers = new ArrayList<AnswerDto>();
        for (Long answerId : answersId) {
            var answer = answerRepo.findById(answerId);
            answer.ifPresent(value -> answers.add(AnswerDto.fromAnswer(value)));
        }
        if (answers.isEmpty()) {
            throw new NotFoundException("");
        }
        return answers;
    }
}
