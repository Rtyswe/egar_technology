package org.example.service.implementations;

import org.example.dtos.FileDto;
import org.example.models.File;
import org.example.repositories.FileRepo;
import org.example.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;

@Service
public class FileServiceImplementation implements FileService {

    private final FileRepo fileRepo;

    @Autowired
    public FileServiceImplementation(FileRepo fileRepo) {
        this.fileRepo = fileRepo;
    }

    @Override
    public void store(MultipartFile file, Long lectureId) throws IOException {
        var filename = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        var saveFile = new File(filename, file.getContentType(), file.getBytes(), lectureId);

        var oldFile = fileRepo.findByLectureId(lectureId).orElse(null);
        if (oldFile != null) {
            throw new BadCredentialsException("File already exist");
        }

        fileRepo.save(saveFile);
    }

    @Override
    public FileDto getFileByLecture(Long lectureId) throws FileNotFoundException {
        var file = fileRepo.findByLectureId(lectureId).orElse(null);
        if (file == null) {
            throw new FileNotFoundException("The file for this lecture does not exist");
        }
        return FileDto.fromFile(file);
    }
}
