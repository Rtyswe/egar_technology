package org.example.rest;

import javassist.NotFoundException;
import org.example.dtos.*;
import org.example.exceptions.LectureCreateException;
import org.example.security.jwt.JwtUser;
import org.example.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping(value = "api/teacher/")
@PreAuthorize("hasAuthority('TEACHER')")
public class TeacherRestController {

    private final LectureService lectureService;
    private final TestService testService;
    private final UserService userService;
    private final AnswerService answerService;
    private final FileService fileService;

    @Autowired
    public TeacherRestController(LectureService lectureService, TestService testService, UserService userService, AnswerService answerService, FileService fileService) {
        this.lectureService = lectureService;
        this.testService = testService;
        this.userService = userService;
        this.answerService = answerService;
        this.fileService = fileService;
    }

    @GetMapping("/my_lectures")
    public ResponseEntity<Object> getMyLectures(
            @AuthenticationPrincipal JwtUser user) {
        try {
            Long id = userService.findIdByUsername(user.getUsername());

            List<LectureDto> myLectures = lectureService.getMyLectures(id);
            if (myLectures.isEmpty()) {
                return new ResponseEntity<>("You don't have any lectures yet", HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(myLectures, HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/my_lectures")
    public ResponseEntity<Object> createLectures(
            @AuthenticationPrincipal JwtUser user,
            @RequestBody LectureCreateDto lectureCreateDto) {
        try {
            Long id = userService.findIdByUsername(user.getUsername());

            lectureService.create(lectureCreateDto, id);
            return new ResponseEntity<>("The lecture was created successfully", HttpStatus.OK);
        } catch (LectureCreateException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/my_lectures/{id_lecture}")
    public ResponseEntity<Object> getMyLecture(
            @PathVariable(name = "id_lecture") Long lectureId,
            @AuthenticationPrincipal JwtUser user) {
        try {
            Long id = userService.findIdByUsername(user.getUsername());

            var myLecture = lectureService.findById(lectureId);
            if (myLecture == null) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            if (!myLecture.getTeacherId().equals(id)) {
                return new ResponseEntity<>("It's not your lecture", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(myLecture, HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/my_lectures/{id_lecture}")
    public ResponseEntity<Object> updateAccess(
            @PathVariable(name = "id_lecture") Long lectureId,
            @RequestBody UpdateAccessDto dto,
            @AuthenticationPrincipal JwtUser user) {
        try {
            Long id = userService.findIdByUsername(user.getUsername());
            if (lectureService.getMyLectures(id).stream().noneMatch(lectureDto -> lectureDto.getId().equals(lectureId))) {
                return new ResponseEntity<>("You can't change the availability of a file for someone else's lecture", HttpStatus.BAD_REQUEST);
            }

            lectureService.updateAccess(lectureId, dto.getAccess());
            return new ResponseEntity<>(lectureService.findById(lectureId), HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/my_lectures/{id_lecture}/file")
    public ResponseEntity<Object> downloadFile(
            @PathVariable(name = "id_lecture") Long lectureId,
            @AuthenticationPrincipal JwtUser user) {
        try {
            Long id = userService.findIdByUsername(user.getUsername());
            if (lectureService.getMyLectures(id).stream().noneMatch(lectureDto -> lectureDto.getId().equals(lectureId))) {
                return new ResponseEntity<>("You can't download a file to someone else's lecture", HttpStatus.BAD_REQUEST);
            }

            if (fileService.getFileByLecture(lectureId) == null) {
                return new ResponseEntity<>("The file for this lecture does not exist", HttpStatus.BAD_REQUEST);
            }

            return new ResponseEntity<>(fileService.getFileByLecture(lectureId), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/my_lectures/{id_lecture}/file")
    public ResponseEntity<Object> uploadFile(
            @PathVariable(name = "id_lecture") Long lectureId,
            @RequestParam("file") MultipartFile file,
            @AuthenticationPrincipal JwtUser user) {
        try {
            Long id = userService.findIdByUsername(user.getUsername());
            if (lectureService.getMyLectures(id).stream().noneMatch(lectureDto -> lectureDto.getId().equals(lectureId))) {
                return new ResponseEntity<>("You can't save a file to someone else's lecture", HttpStatus.BAD_REQUEST);
            }

            fileService.store(file, lectureId);
            return new ResponseEntity<>("The file was saved successfully", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/my_lectures/{id_lecture}/tests")
    public ResponseEntity<Object> getLectureTests(
            @PathVariable(name = "id_lecture") Long lectureId,
            @AuthenticationPrincipal JwtUser user) {
        try {
            Long id = userService.findIdByUsername(user.getUsername());
            if (lectureService.getMyLectures(id).stream().noneMatch(lectureDto -> lectureDto.getId().equals(lectureId))) {
                return new ResponseEntity<>("You can't watch the tests of someone else's lecture", HttpStatus.BAD_REQUEST);
            }

            testService.findAllByLecture(lectureId);
            return new ResponseEntity<>(testService.findAllByLecture(lectureId), HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/my_lectures/{id_lecture}/tests")
    public ResponseEntity<Object> createTests(
            @PathVariable(name = "id_lecture") Long lectureId,
            @AuthenticationPrincipal JwtUser user,
            @RequestBody List<TestCreateDto> tests) {
        try {
            Long id = userService.findIdByUsername(user.getUsername());
            if (lectureService.getMyLectures(id).stream().noneMatch(lectureDto -> lectureDto.getId().equals(lectureId))) {
                return new ResponseEntity<>("This is not your lecture", HttpStatus.BAD_REQUEST);
            }
            if (tests.isEmpty()) {
                return new ResponseEntity<>("You should at least write something", HttpStatus.BAD_REQUEST);
            }

            testService.create(tests, lectureId);
            return new ResponseEntity<>("Tests were created successfully", HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/my_lectures/{id_lecture}/answers")
    public ResponseEntity<Object> getStudentsAnsweredTest(
            @PathVariable(name = "id_lecture") Long lectureId,
            @AuthenticationPrincipal JwtUser user) {
        try {
            Long id = userService.findIdByUsername(user.getUsername());
            if (lectureService.getMyLectures(id).stream().noneMatch(lectureDto -> lectureDto.getId().equals(lectureId))) {
                return new ResponseEntity<>("This is not your lecture", HttpStatus.BAD_REQUEST);
            }

            List<UserDto> usersDto = answerService.getStudentsAnswered(lectureId);
            if (usersDto == null || usersDto.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(usersDto, HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/my_lectures/{id_lecture}/answers/{id_student}")
    public ResponseEntity<Object> getAnswersStudentOfTest(
            @PathVariable(name = "id_lecture") Long lectureId,
            @PathVariable(name = "id_student") Long studentId,
            @AuthenticationPrincipal JwtUser user) {
        try {
            Long id = userService.findIdByUsername(user.getUsername());
            if (lectureService.getMyLectures(id).stream().noneMatch(lectureDto -> lectureDto.getId().equals(lectureId))) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            List<AnswerDto> answersDto = answerService.getAnswersStudentOfLecture(studentId, lectureId);
            return new ResponseEntity<>(answersDto, HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}