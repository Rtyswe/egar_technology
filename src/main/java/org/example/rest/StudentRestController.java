package org.example.rest;

import org.example.dtos.StudentAnswerDto;
import org.example.dtos.TestDto;
import org.example.security.jwt.JwtUser;
import org.example.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;
import java.util.List;

@RestController
@RequestMapping(value = "/api/student/lectures")
@PreAuthorize("hasAuthority('STUDENT')")
public class StudentRestController {

    private final LectureService lectureService;
    private final FileService fileService;
    private final TestService testService;
    private final AnswerService answerService;
    private final UserService userService;

    @Autowired
    public StudentRestController(LectureService lectureService, FileService fileService, TestService testService, AnswerService answerService, UserService userService) {
        this.lectureService = lectureService;
        this.fileService = fileService;
        this.testService = testService;
        this.answerService = answerService;
        this.userService = userService;
    }

    @GetMapping()
    public ResponseEntity<Object> getLectures() {
        try {
            return new ResponseEntity<>(lectureService.getAllLectures(), HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id_lecture}")
    public ResponseEntity<Object> getLectureInfo(
            @PathVariable("id_lecture") Long lectureId) {
        try {
            return new ResponseEntity<>(lectureService.findById(lectureId), HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id_lecture}/file")
    public ResponseEntity<Object> getLectureFile(
            @PathVariable("id_lecture") Long lectureId) {
        try {
            if (!lectureService.findById(lectureId).isAccess()) {
                return new ResponseEntity<>("The teacher denied access to the lecture file", HttpStatus.OK);
            }

            var fileDto = fileService.getFileByLecture(lectureId);
            if (fileDto == null) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(fileDto, HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (FileNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id_lecture}/tests")
    public ResponseEntity<Object> getLectureTests(
            @PathVariable("id_lecture") Long lectureId) {
        try {
            var lectureDto = lectureService.findById(lectureId);
            if (lectureDto == null) {
                return new ResponseEntity<>("The Lecture not found", HttpStatus.NOT_FOUND);
            }

            List<TestDto> testsDto = testService.findAllByLecture(lectureId);
            if (testsDto.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(testsDto, HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/{id_lecture}/tests/{id_test}")
    public ResponseEntity<Object> answerTest(
            @PathVariable("id_lecture") Long lectureId,
            @PathVariable("id_test") Long testId,
            @AuthenticationPrincipal JwtUser user,
            @RequestBody StudentAnswerDto answerDto) {
        try {
            Long studentId = userService.findIdByUsername(user.getUsername());
            if (testService.findAllByLecture(lectureId).stream().noneMatch(testDto -> testDto.getId().equals(testId))) {
                return new ResponseEntity<>("The test does not apply to this lecture", HttpStatus.BAD_REQUEST);
            }

            answerService.saveAnswer(answerDto, studentId, testId);
            return new ResponseEntity<>("The response was saved successfully", HttpStatus.OK);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
