package org.example.dtos;

import lombok.Data;
import org.example.models.Lecture;

import java.util.Date;

@Data
public class LectureDto {

    private Long id;
    private String topic;
    private Date date;
    private Long teacherId;
    private boolean access;

    public static LectureDto fromLecture(Lecture lecture) {
        var lectureDto = new LectureDto();

        lectureDto.setId(lecture.getId());
        lectureDto.setTopic(lecture.getTopic());
        lectureDto.setDate(lecture.getDate());
        lectureDto.setTeacherId(lecture.getTeacherId());
        lectureDto.setAccess(lecture.isAccess());

        return lectureDto;
    }
}
