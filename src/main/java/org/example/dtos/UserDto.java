package org.example.dtos;

import lombok.Data;
import org.example.models.User;

@Data
public class UserDto {

    private Long id;
    private String username;
    private String firstName;
    private String lastName;

    public static UserDto fromUser(User user){
        var userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());

        return userDto;
    }
}
