package org.example.dtos;

import lombok.Data;

@Data
public class UpdateAccessDto {
    private Boolean access;
}
