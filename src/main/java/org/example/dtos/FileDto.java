package org.example.dtos;

import lombok.Data;
import org.example.models.File;

@Data
public class FileDto {

    private Long id;
    private Long lectureId;
    private String filename;
    private byte[] content;
    private String filetype;

    public static FileDto fromFile(File file) {
        var fileDto = new FileDto();

        fileDto.setId(file.getId());
        fileDto.setLectureId(file.getLectureId());
        fileDto.setFilename(file.getFilename());
        fileDto.setContent(file.getContent());
        fileDto.setFiletype(file.getFiletype());

        return fileDto;
    }
}
