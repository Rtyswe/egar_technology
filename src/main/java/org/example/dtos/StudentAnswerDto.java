package org.example.dtos;

import lombok.Data;
import org.example.models.Answer;

import java.sql.Timestamp;

@Data
public class StudentAnswerDto {
    private String text;

    public Answer toAnswer(Long studentId, Long testId) {
        var answer = new Answer();
        answer.setText(text);
        answer.setAnswerDate(new Timestamp(System.currentTimeMillis()));
        answer.setStudentId(studentId);
        answer.setTestId(testId);

        return answer;
    }
}
