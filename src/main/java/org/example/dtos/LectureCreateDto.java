package org.example.dtos;

import lombok.Data;
import org.example.models.Lecture;

import java.sql.Timestamp;

@Data
public class LectureCreateDto {

    private String topic;
    private boolean access;

    public Lecture toLecture(Long teacherId) {
        var lecture = new Lecture();
        lecture.setTopic(this.topic);
        lecture.setDate(new Timestamp(System.currentTimeMillis()));
        lecture.setTeacherId(teacherId);
        lecture.setAccess(this.access);

        return lecture;
    }
}
