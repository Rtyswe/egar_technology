package org.example.dtos;

import lombok.Data;
import org.example.models.Test;

@Data
public class TestDto {

    private Long id;
    private Long lectureId;
    private String question;

    public static TestDto fromTest(Test test) {
        var testDto = new TestDto();
        testDto.setId(test.getId());
        testDto.setLectureId(test.getLectureId());
        testDto.setQuestion(test.getQuestion());

        return testDto;
    }
}
