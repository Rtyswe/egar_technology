package org.example.dtos;

import lombok.Data;
import org.example.models.Test;

@Data
public class TestCreateDto {

    private String question;

    public Test toTest(Long lectureId) {
        var test = new Test();
        test.setQuestion(question);
        test.setLectureId(lectureId);

        return test;
    }
}
