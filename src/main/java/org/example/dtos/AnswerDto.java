package org.example.dtos;

import lombok.Data;
import org.example.models.Answer;

import java.util.Date;

@Data
public class AnswerDto {

    private Long id;
    private Long studentId;
    private Long testId;
    private Date answerDate;
    private String text;

    public static AnswerDto fromAnswer(Answer answer){
        var answerDto = new AnswerDto();

        answerDto.setId(answer.getId());
        answerDto.setStudentId(answer.getStudentId());
        answerDto.setTestId(answer.getTestId());
        answerDto.setAnswerDate(answer.getAnswerDate());
        answerDto.setText(answer.getText());

        return answerDto;
    }
}
