package org.example.dtos;

import lombok.Data;
import org.example.models.User;

@Data
public class RegistrationRequestDto {

    private String username;
    private String password;
    private String firstName;
    private String lastName;

    public User toUser() {
        var user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }
}
